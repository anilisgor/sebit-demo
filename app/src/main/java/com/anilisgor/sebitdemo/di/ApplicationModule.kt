package com.anilisgor.sebitdemo.di

import android.content.Context
import com.anilisgor.sebitdemo.data.local.SebitDao
import com.anilisgor.sebitdemo.data.local.SebitDatabase
import dagger.Module
import dagger.Provides
import dagger.hilt.InstallIn
import dagger.hilt.android.qualifiers.ApplicationContext
import dagger.hilt.components.SingletonComponent
import javax.inject.Singleton

@Module
@InstallIn(SingletonComponent::class)
class ApplicationModule {


    @Provides
    @Singleton
    fun provideSebitDatabase(@ApplicationContext appContext: Context) =
        SebitDatabase.invoke(appContext)

    @Provides
    @Singleton
    fun provideSebitDao(sebitDb: SebitDatabase): SebitDao = sebitDb.sebitDao()


}
