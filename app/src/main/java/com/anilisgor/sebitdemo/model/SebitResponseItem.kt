package com.anilisgor.sebitdemo.model

import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Ignore
import androidx.room.PrimaryKey
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize
import kotlinx.android.parcel.RawValue

@Parcelize
@Entity
data class SebitResponseItem(
    @ColumnInfo()
    val contentType: String,
    @ColumnInfo()
    @Ignore
    val desciription: String,
    @ColumnInfo()
    @Ignore
    val fileUrl: String,
    @ColumnInfo()
    @SerializedName("images")
    @Ignore
    val galleryImages: List<GalleryImage>,
    @ColumnInfo()
    @Ignore
    val questions: List<Question>,
    @ColumnInfo()
    @Ignore
    val tags: @RawValue List<Tag>,
    @ColumnInfo()
    @PrimaryKey()
    val title: String,
    @ColumnInfo()
    @Ignore
    val videoUrl: String
) : Parcelable {

    var downloadTime: Long = 0L

    constructor(contentType: String, title: String) : this(
        contentType,
        "",
        "",
        emptyList(),
        emptyList(),
        emptyList(),
        title,
        ""
    )
}