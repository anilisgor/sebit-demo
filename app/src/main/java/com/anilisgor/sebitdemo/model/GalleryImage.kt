package com.anilisgor.sebitdemo.model

import android.os.Parcel
import android.os.Parcelable
import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
data class GalleryImage(
    @ColumnInfo()
    val original: String = "",
    @ColumnInfo()
    @PrimaryKey()
    val thumb: String
): Parcelable{

    var downloadTime : Long = 0L

}