package com.anilisgor.sebitdemo.model

data class Tag(
    val html: String,
    val positionToGo: Int
)