package com.anilisgor.sebitdemo.model

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Question(
    val description: String,
    val html: String
): Parcelable