package com.anilisgor.sebitdemo.ui.fragment.questiondetailfragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.JavascriptInterface
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import com.anilisgor.sebitdemo.R
import com.anilisgor.sebitdemo.databinding.FragmentQuestionDetailBinding
import com.anilisgor.sebitdemo.model.Question
import com.anilisgor.sebitdemo.ui.adapter.ViewPagerQuestionAdapter
import dagger.hilt.android.AndroidEntryPoint


@AndroidEntryPoint
class QuestionDetailFragment : Fragment() {
    private lateinit var questionListSingle: List<Question>
    private lateinit var binding: FragmentQuestionDetailBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_question_detail, container, false)
        init()
        return binding.root
    }

    private fun init() {

        arguments?.let {
            questionListSingle = QuestionDetailFragmentArgs.fromBundle(it).questionList.toList()
            val adapter = ViewPagerQuestionAdapter(requireContext(), questionListSingle)
            binding.viewPagerQuestion.offscreenPageLimit = 0
            binding.viewPagerQuestion.adapter = adapter


            binding.viewPagerQuestion.setCurrentItem(
                0,
                true
            )
        }

    }

    @JavascriptInterface
    fun showPreloader() {

    }

    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar = (requireActivity() as AppCompatActivity).supportActionBar
        supportActionBar?.show()
    }
}
