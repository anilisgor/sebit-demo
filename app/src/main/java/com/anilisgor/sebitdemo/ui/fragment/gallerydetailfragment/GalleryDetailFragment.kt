package com.anilisgor.sebitdemo.ui.fragment.gallerydetailfragment

import android.app.DownloadManager
import android.content.IntentFilter
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import com.anilisgor.sebitdemo.R
import com.anilisgor.sebitdemo.databinding.FragmentGalleryDetailBinding
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.util.DownloadReceiver
import com.anilisgor.sebitdemo.util.ImageDownloadManager
import com.anilisgor.sebitdemo.util.State
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi

@ExperimentalCoroutinesApi
@AndroidEntryPoint
class GalleryDetailFragment : Fragment() {
    private var thumb = ""
    private var mReceiver: DownloadReceiver? = null
    private val viewModel: GalleryDetailViewModel by viewModels()
    private lateinit var binding: FragmentGalleryDetailBinding
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_gallery_detail, container, false)
        init()
        observeDataStatus()
        return binding.root
    }

    override fun onStart() {
        super.onStart()
        val intentFilter = IntentFilter()
        intentFilter.addAction(DownloadManager.ACTION_DOWNLOAD_COMPLETE)
        requireActivity().registerReceiver(mReceiver, intentFilter)
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        requireActivity().unregisterReceiver(mReceiver)
        val supportActionBar = (requireActivity() as AppCompatActivity).supportActionBar
        supportActionBar?.show()
    }

    private fun init() {
        mReceiver = DownloadReceiver()
        arguments?.let {
            thumb = GalleryDetailFragmentArgs.fromBundle(it).response
            binding.imageUrl = GalleryDetailFragmentArgs.fromBundle(it).response

        }
        binding.buttonDownloadImage.setOnClickListener {
            viewModel.checkAndDownloadImage(GalleryImage(thumb = thumb))
        }
    }

    private fun observeDataStatus() {

        viewModel.checkImageDownloadState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is State.Loading -> {

                }
                is State.Success -> {
                    ImageDownloadManager.downloadImageClicked(it.data.thumb, requireActivity())
                }
                is State.Error -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }

            }
        })
    }

}