package com.anilisgor.sebitdemo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anilisgor.sebitdemo.databinding.ItemChildGalleryBinding
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.ui.listener.HomePageEventListener

class GalleryRecyclerViewAdapter(private val homePageEventListener: HomePageEventListener) :
    ListAdapter<GalleryImage, RecyclerView.ViewHolder>(ImageDiffCallBack()) {

    inner class PopularMoviesViewHolder(private val binding: ItemChildGalleryBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: GalleryImage, homePageEventListener: HomePageEventListener) {
            binding.apply {
                this.images = item
                layoutContent.setOnClickListener {
                    homePageEventListener.openGalleryPicInSecondFragment(item)
                }
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PopularMoviesViewHolder {
        return PopularMoviesViewHolder(
            ItemChildGalleryBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val popularMovies = getItem(position)

        (holder as PopularMoviesViewHolder).bind(popularMovies, homePageEventListener)
    }


}

private class ImageDiffCallBack : DiffUtil.ItemCallback<GalleryImage>() {

    override fun areItemsTheSame(
        oldItem: GalleryImage,
        newItem: GalleryImage
    ): Boolean {
        return oldItem.thumb == newItem.thumb
    }

    override fun areContentsTheSame(
        oldItem: GalleryImage,
        newItem: GalleryImage
    ): Boolean {
        return oldItem == newItem
    }
}


