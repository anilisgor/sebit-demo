package com.anilisgor.sebitdemo.ui.fragment.videodetailfragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.LinearLayoutManager
import com.anilisgor.sebitdemo.R
import com.anilisgor.sebitdemo.databinding.FragmentVideoDetailBinding
import com.anilisgor.sebitdemo.model.SebitResponseItem
import com.anilisgor.sebitdemo.ui.adapter.VideoTagRecyclerViewAdapter


class VideoDetailFragment : Fragment() {
    private lateinit var binding: FragmentVideoDetailBinding
    private val videoTagRecyclerViewAdapter = VideoTagRecyclerViewAdapter()
    private lateinit var item: SebitResponseItem
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding =
            DataBindingUtil.inflate(inflater, R.layout.fragment_video_detail, container, false)
        init()
        return binding.root
    }

    @SuppressLint("SetJavaScriptEnabled")
    private fun init() {
        arguments?.let {
            item = VideoDetailFragmentArgs.fromBundle(it).videos
            binding.webViewVideo.settings.javaScriptEnabled = true
            binding.webViewVideo.loadUrl(item.videoUrl)
        }
        binding.recyclerViewVideoTag.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = videoTagRecyclerViewAdapter
            videoTagRecyclerViewAdapter.submitList(item.tags)
        }
    }

    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar = (requireActivity() as AppCompatActivity).supportActionBar
        supportActionBar?.show()
    }


}