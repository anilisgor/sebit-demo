package com.anilisgor.sebitdemo.ui.fragment.homefragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.appcompat.app.ActionBar
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import com.anilisgor.sebitdemo.R
import com.anilisgor.sebitdemo.databinding.FragmentHomeBinding
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.model.Question
import com.anilisgor.sebitdemo.model.SebitResponseItem
import com.anilisgor.sebitdemo.ui.adapter.HomeRecyclerViewAdapter
import com.anilisgor.sebitdemo.ui.listener.HomePageEventListener
import com.anilisgor.sebitdemo.util.PdfDownloadManager
import com.anilisgor.sebitdemo.util.State
import dagger.hilt.android.AndroidEntryPoint
import kotlinx.coroutines.ExperimentalCoroutinesApi


@ExperimentalCoroutinesApi
@AndroidEntryPoint
class HomeFragment : Fragment(), HomePageEventListener {
    private lateinit var binding: FragmentHomeBinding
    private val homeRecyclerViewAdapter = HomeRecyclerViewAdapter(this)
    private val viewModel: HomeFragmentViewModel by viewModels()
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_home, container, false)
        init()
        return binding.root

    }

    private fun init() {
        binding.recyclerViewMain.apply {
            setHasFixedSize(true)
            layoutManager = LinearLayoutManager(context)
            adapter = homeRecyclerViewAdapter
        }
        viewModel.getData()
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        observeDataStatus()
    }

    private fun observeDataStatus() {
        viewModel.homePageContentLiveData.observe(viewLifecycleOwner, Observer {
            when (it) {
                is State.Loading -> {

                }
                is State.Success -> {
                    homeRecyclerViewAdapter.items = it.data
                    homeRecyclerViewAdapter.notifyDataSetChanged()
                }
                is State.Error -> {

                }

            }
        })
        viewModel.checkPdfDownloadState.observe(viewLifecycleOwner, Observer {
            when (it) {
                is State.Loading -> {

                }
                is State.Success -> {
                    PdfDownloadManager.downloadPdfClicked(it.data, requireActivity())
                }
                is State.Error -> {
                    Toast.makeText(context, it.message, Toast.LENGTH_SHORT).show()
                }

            }
        })
    }

    override fun onDownloadPdfClicked(sebitResponseItem: SebitResponseItem) {
        viewModel.checkAndDownloadPdf(sebitResponseItem)
    }


    override fun openGalleryPicInSecondFragment(galleryImage: GalleryImage) {
        val action =
            HomeFragmentDirections.actionHomeFragmentToGalleryDetailFragment(galleryImage.thumb)
        Navigation.findNavController(binding.root).navigate(action)

    }

    override fun openQuestionFromHomePage(questionList: List<Question>) {
        val action =
            HomeFragmentDirections.actionHomeFragmentToQuestionDetailFragment(questionList.toTypedArray())
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun openVideoFromHomePage(sebitResponseItem: SebitResponseItem) {
        val action =
            HomeFragmentDirections.actionHomeFragmentToVideoDetailFragment(sebitResponseItem)
        Navigation.findNavController(binding.root).navigate(action)
    }

    override fun onResume() {
        super.onResume()
        val supportActionBar: ActionBar? = (requireActivity() as AppCompatActivity).supportActionBar
        if (supportActionBar != null) supportActionBar.hide()
    }

    override fun onStop() {
        super.onStop()
        val supportActionBar = (requireActivity() as AppCompatActivity).supportActionBar
        supportActionBar?.show()
    }
}