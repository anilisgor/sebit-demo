package com.anilisgor.sebitdemo.ui.fragment.gallerydetailfragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anilisgor.sebitdemo.data.repository.SebitRepository
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.util.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class GalleryDetailViewModel @Inject constructor(private val sebitRepository: SebitRepository) :
    ViewModel() {

    private val _checkImageDownloadState = MutableLiveData<State<GalleryImage>>()
    val checkImageDownloadState: LiveData<State<GalleryImage>>
        get() = _checkImageDownloadState

    fun checkAndDownloadImage(galleryImage: GalleryImage) {
        viewModelScope.launch {
            sebitRepository.checkAndDownloadImage(galleryImage).collect {
                _checkImageDownloadState.postValue(it)
            }
        }

    }
}