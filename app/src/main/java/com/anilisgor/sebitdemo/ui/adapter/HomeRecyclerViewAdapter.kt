package com.anilisgor.sebitdemo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewbinding.ViewBinding
import com.anilisgor.sebitdemo.R
import com.anilisgor.sebitdemo.databinding.ItemParentGalleryBinding
import com.anilisgor.sebitdemo.databinding.ItemParentQuestionBinding
import com.anilisgor.sebitdemo.databinding.ItemParentVideosBinding
import com.anilisgor.sebitdemo.databinding.ItemPdfBinding
import com.anilisgor.sebitdemo.model.SebitResponseItem
import com.anilisgor.sebitdemo.ui.listener.HomePageEventListener


class HomeRecyclerViewAdapter(private val homePageEventListener: HomePageEventListener) :
    RecyclerView.Adapter<HomeRecyclerViewAdapter.HomeRecyclerViewHolder>() {


    var items = listOf<SebitResponseItem>()
        set(value) {
            field = value
            notifyDataSetChanged()
        }

    sealed class HomeRecyclerViewHolder(binding: ViewBinding) :
        RecyclerView.ViewHolder(binding.root) {

        class PdfViewHolder(private val binding: ItemPdfBinding) : HomeRecyclerViewHolder(binding) {
            fun bind(
                pdf: SebitResponseItem,
                position: Int,
                homePageEventListener: HomePageEventListener
            ) {
                binding.textViewPdfDescription.text = pdf.desciription
                binding.textViewPdfTitle.text = pdf.title
                binding.imageViewPdfDownload.setOnClickListener {
                    homePageEventListener.onDownloadPdfClicked(pdf)
                }

            }
        }

        class GalleryViewHolder(private val binding: ItemParentGalleryBinding) :
            HomeRecyclerViewHolder(binding) {
            fun bind(
                gallery: SebitResponseItem,
                position: Int,
                homePageEventListener: HomePageEventListener
            ) {
                val galleryRecyclerViewAdapter = GalleryRecyclerViewAdapter(homePageEventListener)
                binding.recyclerViewChildGallery.apply {
                    setHasFixedSize(true)
                    layoutManager =
                        LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
                    adapter = galleryRecyclerViewAdapter
                }

                galleryRecyclerViewAdapter.submitList(gallery.galleryImages)
                binding.textViewGalleryTitle.text = gallery.title
            }


        }

        class QuestionViewHolder(private val binding: ItemParentQuestionBinding) :
            HomeRecyclerViewHolder(binding) {
            fun bind(
                question: SebitResponseItem,
                position: Int,
                homePageEventListener: HomePageEventListener
            ) {
                binding.textViewQuestionTitle.text = question.title
                binding.linearLayoutQuestionList.setOnClickListener {
                    homePageEventListener.openQuestionFromHomePage(question.questions)
                }

            }
        }

        class VideoViewHolder(private val binding: ItemParentVideosBinding) :
            HomeRecyclerViewHolder(binding) {
            fun bind(
                video: SebitResponseItem,
                position: Int,
                homePageEventListener: HomePageEventListener
            ) {
                binding.textViewVideosTitle.text = video.title
                binding.linearLayoutVideo.setOnClickListener {
                    homePageEventListener.openVideoFromHomePage(video)
                }

            }
        }
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): HomeRecyclerViewHolder {
        return when (viewType) {
            R.layout.item_pdf -> HomeRecyclerViewHolder.PdfViewHolder(
                ItemPdfBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            R.layout.item_parent_gallery -> HomeRecyclerViewHolder.GalleryViewHolder(
                ItemParentGalleryBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            R.layout.item_parent_question -> HomeRecyclerViewHolder.QuestionViewHolder(
                ItemParentQuestionBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            R.layout.item_parent_videos -> HomeRecyclerViewHolder.VideoViewHolder(
                ItemParentVideosBinding.inflate(
                    LayoutInflater.from(parent.context),
                    parent,
                    false
                )
            )
            else -> throw IllegalArgumentException("Invalid ViewType Provided")
        }
    }


    override fun onBindViewHolder(holder: HomeRecyclerViewHolder, position: Int) {
        when (holder) {
            is HomeRecyclerViewHolder.PdfViewHolder -> holder.bind(
                items[position],
                position,
                homePageEventListener
            )
            is HomeRecyclerViewHolder.GalleryViewHolder -> holder.bind(
                items[position],
                position,
                homePageEventListener
            )
            is HomeRecyclerViewHolder.QuestionViewHolder -> holder.bind(
                items[position],
                position,
                homePageEventListener
            )
            is HomeRecyclerViewHolder.VideoViewHolder -> holder.bind(
                items[position],
                position,
                homePageEventListener
            )
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (items[position].contentType) {
            "pdf" -> R.layout.item_pdf
            "gallery" -> R.layout.item_parent_gallery
            "question" -> R.layout.item_parent_question
            "video" -> R.layout.item_parent_videos
            else -> {
                return 0
            }
        }
    }

    override fun getItemCount() = items.size
}


