package com.anilisgor.sebitdemo.ui.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.webkit.WebView
import android.webkit.WebViewClient
import androidx.viewpager.widget.PagerAdapter
import com.anilisgor.sebitdemo.R
import com.anilisgor.sebitdemo.model.Question


class ViewPagerQuestionAdapter(
    private val mContext: Context,
    private val questionList: List<Question>
) : PagerAdapter() {

    override fun instantiateItem(collection: ViewGroup, position: Int): Any {
        val modelObject = questionList
        val inflater = LayoutInflater.from(mContext)
        val layout =
            inflater.inflate(R.layout.viewpage_item_question, collection, false) as ViewGroup
        val webViewQuestion = layout.findViewById(R.id.webViewQuestion) as WebView
        webViewQuestion.settings.javaScriptEnabled = true;
        // webViewQuestion.setWebChromeClient(MyWebChromeClient(mContext as Activity?))
        // binding.webViewQuestion.addJavascriptInterface(testClass(), "jsinterface")
        webViewQuestion.loadDataWithBaseURL(
            null, questionList[position].html,
            "text/html", "UTF-8", null
        )

        webViewQuestion.webViewClient = (object : WebViewClient() {
            override fun onPageFinished(view: WebView, url: String) {
                //webViewQuestion.loadUrl("javascript:showPreloader()");
            }
        })

        collection.addView(layout)
        return layout
    }

    override fun destroyItem(collection: ViewGroup, position: Int, view: Any) {
        collection.removeView(view as View)
    }

    override fun getCount(): Int {
        return questionList.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view === `object`
    }


}