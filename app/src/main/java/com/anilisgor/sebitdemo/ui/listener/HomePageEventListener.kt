package com.anilisgor.sebitdemo.ui.listener

import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.model.Question
import com.anilisgor.sebitdemo.model.SebitResponseItem

interface HomePageEventListener {

    fun onDownloadPdfClicked(sebitResponseItem: SebitResponseItem)
    fun openGalleryPicInSecondFragment(galleryImage: GalleryImage)
    fun openQuestionFromHomePage(questionList: List<Question>)
    fun openVideoFromHomePage(sebitResponseItem: SebitResponseItem)

}