package com.anilisgor.sebitdemo.ui.fragment.homefragment

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.anilisgor.sebitdemo.data.repository.SebitRepository
import com.anilisgor.sebitdemo.model.SebitResponseItem
import com.anilisgor.sebitdemo.util.State
import dagger.hilt.android.lifecycle.HiltViewModel
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.collect
import kotlinx.coroutines.launch
import javax.inject.Inject

@ExperimentalCoroutinesApi
@HiltViewModel
class HomeFragmentViewModel @Inject constructor(private val sebitRepository: SebitRepository) :
    ViewModel() {

    private val _homePageContentLiveData = MutableLiveData<State<List<SebitResponseItem>>>()
    val homePageContentLiveData: LiveData<State<List<SebitResponseItem>>>
        get() = _homePageContentLiveData

    private val _checkPdfDownloadState = MutableLiveData<State<SebitResponseItem>>()
    val checkPdfDownloadState: LiveData<State<SebitResponseItem>>
        get() = _checkPdfDownloadState

    fun getData() {
        viewModelScope.launch {
            sebitRepository.fetchHomePageData().collect {
                _homePageContentLiveData.postValue(it)
            }
        }
    }

    fun checkAndDownloadPdf(sebitResponseItem: SebitResponseItem) {
        viewModelScope.launch {
            sebitRepository.checkAndDownloadPdf(sebitResponseItem).collect {
                _checkPdfDownloadState.postValue(it)
            }
        }

    }


}