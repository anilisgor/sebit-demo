package com.anilisgor.sebitdemo.ui.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.ListAdapter
import androidx.recyclerview.widget.RecyclerView
import com.anilisgor.sebitdemo.databinding.ItemChildVideoTagBinding
import com.anilisgor.sebitdemo.model.Tag

class VideoTagRecyclerViewAdapter :
    ListAdapter<Tag, RecyclerView.ViewHolder>(TagDiffCallBack()) {

    inner class VideoTagRecyclerViewHolder(private val binding: ItemChildVideoTagBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(item: Tag) {
            binding.apply {
                this.tag = item


                binding.webViewVideoTag.loadDataWithBaseURL(
                    null, item.html,
                    "text/html", "UTF-8", null
                )
                executePendingBindings()
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): VideoTagRecyclerViewHolder {
        return VideoTagRecyclerViewHolder(
            ItemChildVideoTagBinding.inflate(
                LayoutInflater.from(parent.context), parent, false
            )
        )
    }


    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        val tagsVideo = getItem(position)

        (holder as VideoTagRecyclerViewHolder).bind(tagsVideo)
    }


}

private class TagDiffCallBack : DiffUtil.ItemCallback<Tag>() {

    override fun areItemsTheSame(
        oldItem: Tag,
        newItem: Tag
    ): Boolean {
        return oldItem.html == newItem.html
    }

    override fun areContentsTheSame(
        oldItem: Tag,
        newItem: Tag
    ): Boolean {
        return oldItem == newItem
    }
}