package com.anilisgor.sebitdemo.data.local

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.model.SebitResponseItem

@Dao
interface SebitDao {

    @Insert
    suspend fun insert(vararg sebitResponseItem: SebitResponseItem)

    @Query("SELECT COUNT(title) FROM sebitresponseitem WHERE title = :title AND downloadTime > :systemMilliSec")
    suspend fun checkPdfDownloadState(title : String, systemMilliSec : Long) :  Int

    @Query("DELETE FROM sebitresponseitem")
    suspend fun deleteALlPdf()

    @Insert
    suspend fun insertGalleryImage(vararg galleryImage: GalleryImage)

    @Query("SELECT COUNT(thumb) FROM galleryimage  WHERE thumb = :title AND downloadTime > :systemMilliSec")
    suspend fun checkImageDownloadState(title : String, systemMilliSec : Long) :  Int

    @Query("DELETE FROM galleryimage")
    suspend fun deleteAllGalleryImages()
}