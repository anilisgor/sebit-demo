package com.anilisgor.sebitdemo.data.remote

import com.anilisgor.sebitdemo.model.SebitResponseItem
import retrofit2.http.GET

interface ApiInterface {

    @GET("androiddata.json")
    suspend  fun getPopularMovies() :List<SebitResponseItem>

}