package com.anilisgor.sebitdemo.data.local

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.model.SebitResponseItem

@Database(entities = arrayOf(SebitResponseItem::class, GalleryImage::class), version = 1)
abstract class SebitDatabase : RoomDatabase() {
    abstract fun sebitDao(): SebitDao

    companion object {
        @Volatile
        private var instance: SebitDatabase? = null

        private val lock = Any()
        operator fun invoke(context: Context) = instance ?: synchronized(lock) {
            instance ?: makeDatabase(context).also {
                instance = it
            }
        }

        private fun makeDatabase(context: Context) = Room.databaseBuilder(
            context.applicationContext,
            SebitDatabase::class.java,
            "sebitdatabase"
        ).build()
    }
}