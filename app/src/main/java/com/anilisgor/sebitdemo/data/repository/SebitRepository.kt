package com.anilisgor.sebitdemo.data.repository

import com.anilisgor.sebitdemo.data.local.SebitDao
import com.anilisgor.sebitdemo.data.remote.ApiInterface
import com.anilisgor.sebitdemo.model.GalleryImage
import com.anilisgor.sebitdemo.model.SebitResponseItem
import com.anilisgor.sebitdemo.util.Constant
import com.anilisgor.sebitdemo.util.State
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.ExperimentalCoroutinesApi
import kotlinx.coroutines.flow.Flow
import kotlinx.coroutines.flow.catch
import kotlinx.coroutines.flow.flow
import kotlinx.coroutines.flow.flowOn
import javax.inject.Inject
import javax.inject.Singleton


@Singleton
@ExperimentalCoroutinesApi
class SebitRepository @Inject constructor(
    private val apiService: ApiInterface,
    private val sebitDao: SebitDao
) {

    fun fetchHomePageData(): Flow<State<List<SebitResponseItem>>> {
        return flow<State<List<SebitResponseItem>>> {
            emit(State.loading())
            val response  = apiService.getPopularMovies()
            if(response.isNotEmpty()){
                emit(State.success(response))
            }

            else
                emit(State.error("Bağlantı Sorunu"))
        }.flowOn(Dispatchers.IO)
            .catch { e ->
                emit(State.error("An error occurred."))
            }
    }

    fun checkAndDownloadPdf (sebitResponseItem: SebitResponseItem): Flow<State<SebitResponseItem>> {
        return flow<State<SebitResponseItem>> {
            val value = sebitDao.checkPdfDownloadState(sebitResponseItem.title,
                System.currentTimeMillis()-Constant.TENMINUTES)
            if (value>0){
                emit(State.error("Bir süre daha beklemelisiniz."))
            }else{
                sebitDao.deleteALlPdf()
                sebitResponseItem.downloadTime = System.currentTimeMillis()
                sebitDao.insert(sebitResponseItem)
                emit(State.success(sebitResponseItem))
            }

        }.flowOn(Dispatchers.IO)
            .catch { e ->
                emit(State.error("An error occurred."))
            }
    }

    fun checkAndDownloadImage (galleryImage: GalleryImage): Flow<State<GalleryImage>> {
        return flow<State<GalleryImage>> {
            val value = sebitDao.checkImageDownloadState(galleryImage.thumb,
                System.currentTimeMillis()-Constant.TENMINUTES)
            if (value>0){
                emit(State.error("Bir süre daha beklemelisiniz."))
            }else{
                sebitDao.deleteAllGalleryImages()
                galleryImage.downloadTime = System.currentTimeMillis()
                sebitDao.insertGalleryImage(galleryImage)
                emit(State.success(galleryImage))
            }

        }.flowOn(Dispatchers.IO)
            .catch {e->
                emit(State.error(e.message ?: "error"))
            }
    }



}
