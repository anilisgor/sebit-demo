package com.anilisgor.sebitdemo.util

import android.app.DownloadManager
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.widget.Toast


class DownloadReceiver : BroadcastReceiver() {
    override fun onReceive(p0: Context?, p1: Intent) {
        var action: String = p1.action.toString()

        if (DownloadManager.ACTION_DOWNLOAD_COMPLETE == action) {
            Toast.makeText(p0, "Dosya indirildi.", Toast.LENGTH_SHORT).show()
        }
    }
}