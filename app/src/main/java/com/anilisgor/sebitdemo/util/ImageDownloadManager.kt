package com.anilisgor.sebitdemo.util

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import java.io.File


object ImageDownloadManager {
    fun downloadImageClicked(url: String, activity: Activity) {
        val downloadmanager =
            activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
        val uri: Uri = Uri.parse(url)
        val request = DownloadManager.Request(uri)
        request.setTitle(url)
        request.setMimeType("image/jpeg")
        request.setDescription("$url indiriliyor..")
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setAllowedOverRoaming(false)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setVisibleInDownloadsUi(false)
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_PICTURES,
            File.separator.toString() + url
        )
        downloadmanager!!.enqueue(request)

    }

}