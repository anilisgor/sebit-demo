package com.anilisgor.sebitdemo.util

import android.app.Activity
import android.app.DownloadManager
import android.content.Context
import android.net.Uri
import android.os.Environment
import com.anilisgor.sebitdemo.model.SebitResponseItem

object PdfDownloadManager {

    fun downloadPdfClicked(sebitResponseItem: SebitResponseItem, activity: Activity) {
        val downloadmanager =
            activity.getSystemService(Context.DOWNLOAD_SERVICE) as DownloadManager?
        val uri: Uri = Uri.parse(sebitResponseItem.fileUrl)
        val request = DownloadManager.Request(uri)
        request.setTitle(sebitResponseItem.title)
        request.setDescription(sebitResponseItem.title + " indiriliyor..")
        request.setAllowedNetworkTypes(DownloadManager.Request.NETWORK_WIFI or DownloadManager.Request.NETWORK_MOBILE)
        request.setAllowedOverRoaming(false)
        request.setNotificationVisibility(DownloadManager.Request.VISIBILITY_VISIBLE_NOTIFY_COMPLETED)
        request.setVisibleInDownloadsUi(false)
        request.setDestinationInExternalPublicDir(
            Environment.DIRECTORY_DOWNLOADS, "/" + uri.lastPathSegment
        )
        downloadmanager!!.enqueue(request)

    }
}